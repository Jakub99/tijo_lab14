package pl.edu.pwsztar.domain.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PeselValidator {

    public static boolean isValid(final String pesel) {
        int sum = 0;

        int[] intArray = new int[]{ 1,3,7,9,1,3,7,9,1,3,0 };

        if(pesel == null) {
            return false;
        }

        for (int i = 0; i < pesel.length() - 1; i++) {
            sum += (Character.getNumericValue(pesel.charAt(i)) * intArray[i]);
        }

        if(sum % 10 == 10 && pesel.charAt(pesel.length() - 1) == 0 ) {
            return true;
        }

        if(10 - (sum % 10) == pesel.charAt(pesel.length() - 1)) {
            return true;
        }

        return false;
    }
}
